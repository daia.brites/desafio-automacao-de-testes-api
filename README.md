# Desafio Automação de Testes - API

Automação de testes para uma API que contém CPFs com restrições e simulação de crédito.

## Preparação 
Foram realizados testes funcionais no documento Swagger, para identificar os retornos das chamadas e obter entendimento sobre as requests de restrição e simulação de crédito.

A técnica utilizada foi teste de contrato, pois é baseado na saída (response) gerada após a requisição.
Foram realizados testes comparando: URL destino, dados de retorno em formado json e código de status HTTP do retorno. Desta forma, foram desenvolvidos 17 cenários de testes.

Foram utilizadas as seguintes tecnologias para criar esta automação de testes:
- Maven
- Java 1.8
- Junit
- Restassured
- Javafaker - Utilizado para criar dados randômicos(não fixos)
- Allure Report

-------------
## Como executar os testes da automação

`mvn clean test`

-----------------------
## Como executar o Allure

Para gerar os relatórios de testes do Allure, é necessário fazer a instalação do Allure conforme a documentação oficial: https://docs.qameta.io/allure/#_installing_a_commandline

Para gerar o relatório do Allure é necessário rodar todos os testes da automação e depois executar a seguinte linha de comando na pasta da automação:

`allure serve target\allure-results`

Print do relatório do Allure após executar o comando:

![allure-reports.png](allure-reports.png)

-------------------------
# Considerações finais sobre automação de API

## Respostas ou comportamentos diferentes do que informa a documentação da prova:

`GET /restricoes/{cpf}`

A documentação informa: Se possui restrição o HTTP Status 200 é retornado com a mensagem "O CPF 99999999999 possui restrição"
porém, está retornando a mensagem "O CPF 99999999999 tem problema".


`POST /simulacoes`

A documentação informa que alguns campos possuem regras de validação, porém algumas regras estão incorretas conforme a lista abaixo:
- Não está validando quando o campo "parcela" é maior que 48.
- Não está validando quando o campo "valor" é inferior que 1000.
- Está permitindo informar um número no campo "seguro" e mesmo assim salvando com sucesso o conteúdo 'true'.
- Está permitindo informar uma String no campo "seguro", por exemplo "sim", e retornando HTTP Status 400 sem mensagem com erro/validação obtido.
- Está permitindo informar uma String no campo "cpf", por exemplo "Av Brasil", e criando a simulação de crédito com sucesso.

Observação: Por estes motivos, foi criado o teste "Criar uma simulação informando 4 campos fora das regras de validação", o qual está falhando, pois as validações da API precisam ser corrigidas.

`POST /simulacoes`

A documentação informa: Uma simulação com problema em alguma regra retorna o HTTP Status 400 com a lista de erros,
porém, quando não é informado nenhum campo, está retornando HTTP Status 500, mostrando que esse retorno não teve um tratamento. Minha recomendação seria criar um tratamento para este tipo de requisição (quando não é informado nenhum campo), apresentando status e a mensagem de erro informando o usuário. 


`POST /simulacoes`

A documentação informa: Uma simulação para um mesmo CPF retorna um HTTP Status 409 com a mensagem "CPF já existente",
porém, está retornando HTTP Status 400 com a mensagem "CPF duplicado".

`PUT /simulacoes`

API não está alterando o campo "valor", mesmo retornando HTTP Status 200 confirmando a alteração.

`DELETE /simulacoes`

API está com problema, pois está permitindo deletar IDs que não existem e retorna HTTP Status 200.

Observação: Por este motivo, foi criado o teste "Deletar uma simulação de crédito que não existe (que já foi deletada)", o qual está falhando, pois deveria retornar 404 neste cenário.