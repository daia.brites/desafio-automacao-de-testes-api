package br.com.sicredi.api.creditsimulation.tests.requests;

import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.given;

public class PutCreditSimulationRequest {

    @Step("Alterar dados de uma simulação de crédito")
    public Response putCreditSimulation(String cpf) {
        JSONObject payload = new JSONObject();
        payload.put("nome", "Nome Alterado");
        payload.put("email", "teste@hotmail.com");
        //payload.put("valor", 27500);   -> API não está alterando o campo valor!
        payload.put("parcelas", 89);
        payload.put("seguro", false);

        return given()
                .header("Content-Type", "application/json")
                .when()
                .body(payload.toString())
                .put("simulacoes/" + cpf);
    }
}
