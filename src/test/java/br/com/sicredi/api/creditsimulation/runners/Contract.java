package br.com.sicredi.api.creditsimulation.runners;

import br.com.sicredi.api.creditsimulation.tests.CpfRestrictionTest;
import br.com.sicredi.api.creditsimulation.tests.CreditSimulationTest;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Categories.IncludeCategory(br.com.sicredi.api.creditsimulation.suites.Contract.class)
@Suite.SuiteClasses({
        CpfRestrictionTest.class,
        CreditSimulationTest.class
})

public class Contract {
}

