package br.com.sicredi.api.creditsimulation.tests.requests;

import com.github.javafaker.Faker;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.json.simple.JSONObject;

import static io.restassured.RestAssured.given;

public class PostCreditSimulationResquest {

    private static Faker faker = new Faker();

    public Response postCreditSimulation() {
        return postCreditSimulation(null);
    }

    @Step("Criar uma simulação de crédito")
    public Response postCreditSimulation(String cpf) {
        // Prepara com dados randômicos
        JSONObject payload = new JSONObject();
        // Se deve testar com um CPF específico
        if(cpf != null) {
            payload.put("cpf", cpf);
        } else {
            payload.put("cpf", faker.regexify("\\d{11}"));
        }
        payload.put("nome", faker.name().name());
        payload.put("email", faker.internet().emailAddress());
        payload.put("valor", faker.number().numberBetween(1000,40000));
        payload.put("parcelas", faker.number().numberBetween(2,48));
        payload.put("seguro", faker.bool().bool());

        return given()
                .header("Content-Type", "application/json")
                .when()
                .body(payload.toString())
                .post("simulacoes");
    }

    @Step("Criar uma simulação de crédito sem informar CPF")
    public Response postCreditSimulationWithOutCPF() {
        JSONObject payload = new JSONObject();
        payload.put("nome", "Fulano de Tal");
        payload.put("email", "email@email.com");
        payload.put("valor", 1200);
        payload.put("parcelas", 3);
        payload.put("seguro", true);

        return given()
                .header("Content-Type", "application/json")
                .when()
                .body(payload.toString())
                .post("simulacoes");
    }

    @Step("Criar uma simulação de crédito faltando 5 campos")
    public Response postCreditSimulationWithout5Fields() {
        JSONObject payload = new JSONObject();
        payload.put("seguro", true);

        return given()
                .header("Content-Type", "application/json")
                .when()
                .body(payload.toString())
                .post("simulacoes");
    }

    @Step("Criar uma simulação de crédito com 3 erros")
    public Response postCreditSimulationWith3Errors() {
        JSONObject payload = new JSONObject();

        payload.put("nome", faker.name().name());
        payload.put("cpf", faker.regexify("\\d{11}"));
        payload.put("email", "emailinvalido@gmail");
        payload.put("valor", faker.number().numberBetween(40001, 999999));
        payload.put("parcelas", faker.number().numberBetween(0,1));
        payload.put("seguro", faker.bool().bool());

        return given()
                .header("Content-Type", "application/json")
                .when()
                .body(payload.toString())
                .post("simulacoes");
    }

    @Step("Criar uma simulação de crédito com 4 erros")
    public Response postCreditSimulationWith4Errors() {
        JSONObject payload = new JSONObject();
        payload.put("nome", faker.name().name());
        payload.put("cpf", "cpf_invalido" + faker.random().nextInt(9999999));
        payload.put("email", faker.internet().emailAddress());
        payload.put("valor", faker.number().numberBetween(0,999));
        payload.put("parcelas", faker.number().numberBetween(49,999999));
        payload.put("seguro", faker.number().numberBetween(0,999));

        return given()
                .header("Content-Type", "application/json")
                .when()
                .body(payload.toString())
                .post("simulacoes");
    }
}
