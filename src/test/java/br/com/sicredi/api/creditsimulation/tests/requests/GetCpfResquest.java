package br.com.sicredi.api.creditsimulation.tests.requests;

import com.github.javafaker.Faker;
import io.qameta.allure.Step;
import io.restassured.response.Response;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;

public class GetCpfResquest {

    private static List<String> CPF_RESTRICTION = Arrays.asList("97093236014", "60094146012", "84809766080", "62648716050", "26276298085","01317496094", "55856777050", "19626829001", "24094592008", "58063164083");
    private static Faker faker = new Faker();

    @Step("Consulta CPF com restrição")
    public Response cpfWithRestriction() {
        // Escolhe rancomicamente algum CPF com restrição
        String cpf = faker.options().nextElement(CPF_RESTRICTION);
        return given()
                .when()
                .get("restricoes/" + cpf);
    }

    @Step("Consulta CPF sem restrição")
    public Response cpfWithoutRestriction() {
        return given()
                .when()
                .get("restricoes/46586936071");
    }

    @Step("Consulta CPF inválido")
    public Response cpfInvalid() {
        return given()
                .when()
                .get("restricoes/12345678901");
    }

    @Step("Consulta CPF Incorreto Alfanumérico")
    public Response cpfAlphanumericIncorrect() {
        return given()
                .when()
                .get("restricoes/12ABCD");
    }
}
