package br.com.sicredi.api.creditsimulation.tests.requests;

import io.qameta.allure.Step;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class GetCreditSimulationResquest {

    @Step("Consulta todas as simulações existentes")
    public Response getAllCreditSimulations() {
        return given()
                .when()
                .get("simulacoes");
    }

    @Step("Consulta uma simulação através do CPF")
    public Response getCreditSimulation(String cpf) {
        return given()
                .when()
                .get("simulacoes/" + cpf);
    }
}
