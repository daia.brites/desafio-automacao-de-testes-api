package br.com.sicredi.api.creditsimulation.tests;

import io.restassured.RestAssured;
import org.junit.BeforeClass;

public class BaseTest {

    private static final String PATH_JSON_DIR = "src/test/java/br/com/sicredi/api/creditsimulation/tests/contracts/";

    @BeforeClass
    public static void setup() {
        RestAssured.baseURI = "http://localhost:8080/api/v1/";
   }

    // Retorna o caminha completo do Json
    String getFormatedJsonPath(String jsonFileName){

        return PATH_JSON_DIR + jsonFileName;
    }

}
