package br.com.sicredi.api.creditsimulation.tests.requests;

import io.qameta.allure.Step;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class DeleteCreditSimulationRequest {

    @Step("Deletar uma simulação de crédito")
    public Response deleteCreditSimulation(int id) {

        return given()
                .when()
                .delete("simulacoes/"+id);
    }
}
