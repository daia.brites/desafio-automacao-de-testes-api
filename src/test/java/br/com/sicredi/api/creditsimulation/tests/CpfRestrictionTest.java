package br.com.sicredi.api.creditsimulation.tests;

import br.com.sicredi.api.creditsimulation.suites.Contract;
import br.com.sicredi.api.creditsimulation.tests.requests.GetCpfResquest;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class CpfRestrictionTest extends BaseTest {

    private GetCpfResquest getCpfResquest = new GetCpfResquest();

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Consulta de CPF que possui restrição")
    public void validateCpfWithRestriction() {
        getCpfResquest.cpfWithRestriction().then()
                .statusCode(200)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File(getFormatedJsonPath("CPFsWithRestriction.json"))
                        )
                );
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Category(Contract.class)
    @DisplayName("Consulta de CPF sem restrição")
    public void validateCpfWithoutRestriction() throws Exception {
        getCpfResquest.cpfWithoutRestriction().then()
                .statusCode(204);

    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Category(Contract.class)
    @DisplayName("Consulta de CPF Inválido")
    public void validateCpfInvalid() throws Exception {
        getCpfResquest.cpfInvalid().then()
                .statusCode(204);
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Consulta de CPF incorreto alfanumérico")
    public void validateCpfAlphanumericIncorrect() throws Exception {
        getCpfResquest.cpfAlphanumericIncorrect().then()
                .statusCode(204);
    }

}
