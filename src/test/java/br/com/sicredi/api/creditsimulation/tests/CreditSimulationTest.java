package br.com.sicredi.api.creditsimulation.tests;

import br.com.sicredi.api.creditsimulation.suites.Contract;
import br.com.sicredi.api.creditsimulation.tests.requests.DeleteCreditSimulationRequest;
import br.com.sicredi.api.creditsimulation.tests.requests.GetCreditSimulationResquest;
import br.com.sicredi.api.creditsimulation.tests.requests.PostCreditSimulationResquest;
import br.com.sicredi.api.creditsimulation.tests.requests.PutCreditSimulationRequest;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit4.DisplayName;
import org.json.simple.JSONObject;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.lessThan;

public class CreditSimulationTest extends BaseTest {

    private GetCreditSimulationResquest getCreditSimulationResquest = new GetCreditSimulationResquest();
    private PostCreditSimulationResquest postCreditSimulationResquest = new PostCreditSimulationResquest();
    private DeleteCreditSimulationRequest deleteCreditSimulationRequest = new DeleteCreditSimulationRequest();
    private PutCreditSimulationRequest putCreditSimulationRequest = new PutCreditSimulationRequest();

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Consulta todas as simulações existentes")
    public void getAllCreditSimulation() {
        getCreditSimulationResquest.getAllCreditSimulations().then()
                .statusCode(200)
                .time(lessThan(5L), TimeUnit.SECONDS)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File(getFormatedJsonPath("CreditSimulationArray.json"))
                        )
                );
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Consulta uma simulação através do CPF")
    public void getCreditSimulation() {
        // Primeiro cria uma simulação aleatória e armazena CPF utilizado
        String cpf = postCreditSimulationResquest.postCreditSimulation().then()
                .statusCode(201)
                .time(lessThan(5L), TimeUnit.SECONDS)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File(getFormatedJsonPath("CreditSimulationCreateOK.json"))
                        )
                )
                .extract()
                .path("cpf");
        // Então, busca pela simulação de crédito recém criada CPF
        getCreditSimulationResquest.getCreditSimulation(cpf).then()
                .statusCode(200)
                .time(lessThan(5L), TimeUnit.SECONDS)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File(getFormatedJsonPath("CreditSimulationCreateOK.json"))
                        )
                );
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Consulta uma simulação através do CPF que não possui registro")
    public void getCreditSimulationNotFoundCPF() {
        getCreditSimulationResquest.getCreditSimulation("66414919000").then()
                .statusCode(404)
                .time(lessThan(5L), TimeUnit.SECONDS)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File(getFormatedJsonPath("CreditSimulationNotFoundCPF.json"))
                        )
                );
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Criar uma simulação sem informar CPF na requisição")
    public void createCreditSimulationWithOutCPF() {
        postCreditSimulationResquest.postCreditSimulationWithOutCPF().then()
                .statusCode(400)
                .time(lessThan(5L), TimeUnit.SECONDS)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File(getFormatedJsonPath("CreditSimulationCreateWithoutCPF.json"))
                        )
                );
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Criar uma simulação sem informar 5 campos obrigatórios")
    public void createCreditSimulationWithout5Fields() {
        postCreditSimulationResquest.postCreditSimulationWithout5Fields().then()
                .statusCode(400)
                .time(lessThan(5L), TimeUnit.SECONDS)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File(getFormatedJsonPath("CreditSimulationCreateWith5Errros.json"))
                        )
                );
    }

    /*
    3 campos fora das regras de validação:
    Parcela menor que 2
    Valor maior que R$ 40.000
    E-mail inválido
     */
    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Criar uma simulação informando 3 campos fora das regras de validação")
    public void createCreditSimulationWith3Erros() {
        postCreditSimulationResquest.postCreditSimulationWith3Errors().then()
                .statusCode(400)
                .time(lessThan(5L), TimeUnit.SECONDS)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File(getFormatedJsonPath("CreditSimulationCreateWith3Errors.json"))
                        )
                );
    }

    /*
    4 campos fora das regras de validação:
    CPF informado com letras
    Parcela maior que 48
    Valor menor que R$ 1.000
    Seguro diferente de true/false
     */
    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Criar uma simulação informando 4 campos fora das regras de validação")
    public void createCreditSimulationWith4Erros() {
        postCreditSimulationResquest.postCreditSimulationWith4Errors().then()
                .statusCode(400)
                .time(lessThan(5L), TimeUnit.SECONDS)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File(getFormatedJsonPath("CreditSimulationCreateWith4Errors.json"))
                        )
                );
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Criar uma simulação com sucesso")
    public void createCreditSimulation() {
        postCreditSimulationResquest.postCreditSimulation().then()
                .statusCode(201)
                .time(lessThan(5L), TimeUnit.SECONDS)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File(getFormatedJsonPath("CreditSimulationCreateOK.json"))
                        )
                );
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Criar uma simulação para CPF que já possui simulação de crédito")
    public void createCreditSimulationDuplicate() {
        // Primeiro cria uma simulação aleatória e armazena CPF utilizado
        String cpf = postCreditSimulationResquest.postCreditSimulation().then()
                .statusCode(201)
                .time(lessThan(5L), TimeUnit.SECONDS)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File(getFormatedJsonPath("CreditSimulationCreateOK.json"))
                        )
                )
                .extract()
                .path("cpf");
        // Então, tenta criar uma nova simulação para o mesmo CPF já utilizado
        postCreditSimulationResquest.postCreditSimulation(cpf).then()
                .statusCode(400)
                .time(lessThan(5L), TimeUnit.SECONDS)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File(getFormatedJsonPath("CreditSimulationDuplicated.json"))
                        )
                );
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Deletar uma simulação de crédito")
    public void deleteCreditSimulation() {
        // Primeiro cria uma nova simulação de crédito
        // e armazena o ID e CPF criado para a simulação
        JSONObject response = postCreditSimulationResquest.postCreditSimulation().then()
                .statusCode(201)
                .time(lessThan(5L), TimeUnit.SECONDS)
                .extract()
                .as(JSONObject.class);
        int id = (Integer) response.get("id");
        String cpf = (String) response.get("cpf");
        // Então: Deleta a simulação de crédito através do ID
        deleteCreditSimulationRequest.deleteCreditSimulation(id).then()
                .statusCode(200)
                .time(lessThan(5L), TimeUnit.SECONDS);
        // Valida que a simulação de crédito vinculado ao id foi deletada
        getCreditSimulationResquest.getCreditSimulation(cpf).then()
                .statusCode(404)
                .time(lessThan(5L), TimeUnit.SECONDS);
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Deletar uma simulação de crédito que não existe (que já foi deletada)")
    public void deleteCreditSimulationAgain() {
        // Primeiro cria uma nova simulação de crédito
        // e armazena o ID e CPF criado para a simulação
        JSONObject response = postCreditSimulationResquest.postCreditSimulation().then()
                .statusCode(201)
                .time(lessThan(5L), TimeUnit.SECONDS)
                .extract()
                .as(JSONObject.class);
        int id = (Integer) response.get("id");
        String cpf = (String) response.get("cpf");
        // Então: Deleta a simulação de crédito através do ID
        deleteCreditSimulationRequest.deleteCreditSimulation(id).then()
                .statusCode(200)
                .time(lessThan(5L), TimeUnit.SECONDS);
        // Valida que a simulação de crédito vinculado ao id foi deletada
        getCreditSimulationResquest.getCreditSimulation(cpf).then()
                .statusCode(404)
                .time(lessThan(5L), TimeUnit.SECONDS);
        // Então: Deleta novamente a mesma simulação de crédito através do ID
        deleteCreditSimulationRequest.deleteCreditSimulation(id).then()
                .statusCode(404)
                .time(lessThan(5L), TimeUnit.SECONDS);
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Alterar uma simulação de crédito com sucesso")
    public void editCreditSimulation() {
        putCreditSimulationRequest.putCreditSimulation("17822386034").then()
                .statusCode(200)
                .time(lessThan(5L), TimeUnit.SECONDS)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File(getFormatedJsonPath("CreditSimulationEditOK.json"))
                        )
                );
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Alterar uma simulação de crédito para um CPF sem registro de simulação")
    public void editCreditSimulationCPFNotFound() {
        putCreditSimulationRequest.putCreditSimulation("44436378059").then()
                .statusCode(404)
                .time(lessThan(5L), TimeUnit.SECONDS)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File(getFormatedJsonPath("CreditSimulationNotFoundCPF.json"))
                        )
                );
    }
}

